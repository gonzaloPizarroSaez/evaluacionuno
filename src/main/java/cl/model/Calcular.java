/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.model;

/**
 *
 * @author gps
 */
public class Calcular {

    /**
     * @return the capital
     */
    public double getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(double capital) {
        this.capital = capital;
    }

    /**
     * @return the tasa
     */
    public double getTasa() {
        return tasa;
    }

    /**
     * @param tasa the tasa to set
     */
    public void setTasa(double tasa) {
        this.tasa = tasa;
    }

    /**
     * @return the total
     */
    public double getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(double total) {
        this.total = total;
    }

    /**
     * @return the tiempo
     */
    public double getTiempo() {
        return tiempo;
    }

    /**
     * @param tiempo the tiempo to set
     */
    public void setTiempo(double tiempo) {
        this.tiempo = tiempo;
    }

    /**
     * @return the pt
     */
    public double getPt() {
        return pt;
    }

    /**
     * @param pt the pt to set
     */
    public void setPt(double pt) {
        this.pt = pt;
    }

    /**
  
    /**
     * @return the numero1
     */
    private double capital;
    private double tasa;
    private double total;
    private double tiempo;
    private double pt;

    public void porcentaje() {
        this.pt=this.getTasa()/100;
    }

    public void calcular() {
       
       this.total=this.getCapital()*this.pt*this.getTiempo();
    }

}
